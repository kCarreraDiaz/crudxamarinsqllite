﻿using System;
using System.Collections.Generic;
using System.Text;
using lab14CRUD.Models;
using System.Threading.Tasks;

namespace lab14CRUD.Services
{
    public interface IUser
    {
        Task<List<UserModel>> GetListUsers();
        Task<UserModel> GetUser(int UserId);
        Task<bool> Insert(UserModel userModel);
        Task<bool> Update(UserModel userModel);
        Task<bool> Delete(UserModel userModel);
        Task<bool> DeleteAllUsers();
    }
}

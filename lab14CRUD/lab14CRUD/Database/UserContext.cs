﻿using System;
using System.Collections.Generic;
using System.Text;
using lab14CRUD.Models;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Xamarin.Essentials;

namespace lab14CRUD.Database
{
    public class UserContext : DbContext
    {
        public DbSet<UserModel> TbUsers { get; set; }
        public UserContext()
        {
            SQLitePCL.Batteries_V2.Init();

            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string dbPath = Path.Combine(FileSystem.AppDataDirectory, "UserDB.db3");
            optionsBuilder.UseSqlite($"Filename={dbPath}");
        }
    }
}

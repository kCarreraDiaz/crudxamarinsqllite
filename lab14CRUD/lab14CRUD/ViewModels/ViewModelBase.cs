﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace lab14CRUD.ViewModels
{
    public class ViewModelBase: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged([CallerMemberName] string nameProperty = null)
        {
            var change = PropertyChanged;
            if (change != null)
            {
                change(this, new PropertyChangedEventArgs(nameProperty));
            }
        }
    }
}

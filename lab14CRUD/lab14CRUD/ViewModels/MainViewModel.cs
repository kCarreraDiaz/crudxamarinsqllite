﻿using System;
using System.Collections.Generic;
using System.Text;

using lab14CRUD.Funciones;
using lab14CRUD.Models;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace lab14CRUD.ViewModels
{
    public class MainViewModel: ViewModelBase
    {
        Users users = new Users();

        private ObservableCollection<UserModel> listUsers;
        public ObservableCollection<UserModel> ListUsers
        {
            get { return listUsers; }
            set { listUsers = value; RaisePropertyChanged(); }
        }
        //Variable id
        private int idUserP;
        public int IdUserP
        {
            get { return idUserP; }
            set { idUserP = value; RaisePropertyChanged(); }
        }
        //Variable nombre
        private string nameUserP;
        public string NameUserP
        {
            get { return nameUserP; }
            set { nameUserP = value; RaisePropertyChanged(); }
        }
        //Variable nacimiento
        private DateTime ageUserP;
        public DateTime AgeUserP
        {
            get { return ageUserP; }
            set { ageUserP = value; RaisePropertyChanged(); }
        }
        //Variable activo
        private bool statusP;
        public bool StatusP
        {
            get { return statusP; }
            set { statusP = value; RaisePropertyChanged(); }
        }

        public ICommand InsertRowCommand { get; set; }
        public ICommand UpdateRowCommand { get; set; }
        public ICommand DeleteRowCommand { get; set; }
        public ICommand DeleteAllRowCommand { get; set; }

        public MainViewModel()
        {
            //Eliminar una fila
            DeleteRowCommand = new Command<UserModel>(execute: async (userModel) =>
            {
                await users.Delete(userModel);
                LoadListUsers();
            });

            //Eliminar todo
            DeleteAllRowCommand = new Command(execute: async () =>
            {
                await users.DeleteAllUsers();
                LoadListUsers();
            });

            //actulaizar una fila
            UpdateRowCommand = new Command<UserModel>(execute: async (userModel) =>
            {
                await users.Update(userModel);
                LoadListUsers();
            });

            //Insertar una fila
            InsertRowCommand = new Command(execute: async () =>
            {
                await users.Insert(new UserModel() { IdUserModel = IdUserP, NameUserModel = NameUserP, AgeUserModel = AgeUserP, StatusModel = StatusP });
                IdUserP = 0;
                NameUserP = "";
                AgeUserP = DateTime.Today;
                StatusP = false;
                LoadListUsers();
            });

            LoadListUsers();
        }

        async void LoadListUsers()
        {
            ListUsers = new ObservableCollection<UserModel>(await users.GetListUsers());
        }
    }
}

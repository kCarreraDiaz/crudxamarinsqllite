﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using lab14CRUD.Models;
using lab14CRUD.ViewModels;
using System.Globalization;

namespace lab14CRUD
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        MainViewModel Vm { get { return BindingContext as MainViewModel; } }
        public async void UpdateRow_Tapped(object sender, EventArgs e)
        {
            var contenedor = ((Frame)sender).GestureRecognizers[0];

            UserModel userModel = ((TapGestureRecognizer)contenedor).CommandParameter as UserModel;

            string NameUpt = await DisplayPromptAsync("Username", userModel.NameUserModel);

            DateTime AgeFmt = userModel.AgeUserModel;
            string AgeSrt = AgeFmt.ToString("dd/MM/yyyy");
            string AgeUpt = await DisplayPromptAsync("Fecha de nacimiento", AgeSrt);

            bool StatusFmt = userModel.StatusModel;
            string StatusSrt = StatusFmt.ToString();
            string StatusUpt = await DisplayPromptAsync("Status (True/False)", StatusSrt);

            userModel.NameUserModel = NameUpt;
            string format = "dd/MM/yyyy";
            DateTime AgeFmtDt = DateTime.ParseExact(AgeUpt, format, CultureInfo.InvariantCulture);
            userModel.AgeUserModel = AgeFmtDt;
            bool StatusFmtBl = System.Convert.ToBoolean(StatusSrt);
            Console.WriteLine(StatusFmtBl);
            userModel.StatusModel = StatusFmtBl;

            Vm.UpdateRowCommand.Execute(userModel);

        }

        private async void DeleteRow_Swiped(object sender, SwipedEventArgs e)
        {
            bool result = await DisplayAlert("Eliminar", "Estas seguro?", "Si", "No");
            if (result)
            {
                var container = ((Frame)sender).GestureRecognizers[0];
                UserModel userModel = ((TapGestureRecognizer)container).CommandParameter as UserModel;
                Vm.DeleteRowCommand.Execute(userModel);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using lab14CRUD.Database;
using lab14CRUD.Models;
using lab14CRUD.Services;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace lab14CRUD.Funciones
{
    public class Users: IUser
    {
        //Eliminar
        public async Task<bool> Delete(UserModel userModel)
        {
            using (var usersContext = new UserContext())
            {
                var tracking = usersContext.Remove(userModel);
                await usersContext.SaveChangesAsync();
                var isDeleted = tracking.State == EntityState.Deleted;
                return isDeleted;
            }
        }

        //Delete
        public async Task<bool> DeleteAllUsers()
        {
            using (var usersContext = new UserContext())
            {
                usersContext.RemoveRange(usersContext.TbUsers);
                await usersContext.SaveChangesAsync();
            }
            return true;
        }

        //Obtener lista
        public async Task<List<UserModel>> GetListUsers()
        {
            using (var usersContext = new UserContext())
            {
                return await usersContext.TbUsers.ToListAsync();
            }
        }

        //Obtener
        public async Task<UserModel> GetUser(int UserId)
        {
            using (var usersContext = new UserContext())
            {
                return await usersContext.TbUsers.Where(p => p.IdUserModel == UserId).FirstOrDefaultAsync();
            }
        }

        //CREATE
        public async Task<bool> Insert(UserModel userModel)
        {
            using (var userContext = new UserContext())
            {
                userContext.Add(userModel);
                await userContext.SaveChangesAsync();
            }
            return true;
        }

        //Update
        public async Task<bool> Update(UserModel userModel)
        {
            using (var usersContext = new UserContext())
            {
                var tracking = usersContext.Update(userModel);
                await usersContext.SaveChangesAsync();

                var isModified = tracking.State == EntityState.Modified;
                return isModified;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace lab14CRUD.Models
{
    public class UserModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdUserModel { get; set; }
        public string NameUserModel { get; set; }
        public DateTime AgeUserModel { get; set; }
        public bool StatusModel { get; set; }
    }
}
